# 
# Makefile
# 
# compile exponent unit and test programs
# 
CC=fpc
FLAGS=-g

all: lib testexp testexpmod testexpreal

lib:
	${CC} exponent.pas
testexp: 
	${CC} ${FLAGS} testExp.pas
testexpmod: 
	${CC} ${FLAGS} testExpMod.pas
testexpreal: 
	${CC} ${FLAGS} testExpReal.pas
clean:
	rm testExp testExpMod testExpReal
