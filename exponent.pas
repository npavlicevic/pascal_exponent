unit exponent;
  {do exponentiation for integers and reals}
  interface
    uses Math;
    function exponentFind(a,n: Int64): Int64;
    function exponentFindModular(a,n,m: Int64): Int64;
    function exponentFindReal(a,b: Float): Float;
  implementation
    {
      exponentFind
    
    
      raises an integer to exponent
    }
    function exponentFind(a,n: Int64): Int64;
    var
      run: Int64;
    begin
      run := 1;
      while n > 0 do
        begin
          if (n mod 2) = 1 then
            run := run*a;
          a := a*a;
          n := Trunc(n/2);
        end;
      exponentFind := run;
    end;
    {
      exponentFindModular

      raises integer to exponent mod m
    }
    function exponentFindModular(a,n,m: Int64): Int64;
    var
      run: Int64;
    begin
      run := 1;
      while n > 0 do
        begin
          if (n mod 2) = 1 then
            run :=  (run*a) mod m;
          a := (a*a) mod m;
          n := Trunc(n/2);
        end;
      exponentFindModular := run;
    end;
    {
      exponentFindReal

      raises a real to exponent
    }
    function exponentFindReal(a,b: Float): Float;
    begin
      exponentFindReal := power(a,b);
    end;
end.
