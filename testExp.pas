program testExp;
  uses sysutils, exponent;
  var
    a,n,run: Int64;
  begin
    a := StrToInt(ParamStr(1));
    n := StrToInt(ParamStr(2));
    run := exponentFind(a,n);
    writeln(IntToStr(run));
  end.
