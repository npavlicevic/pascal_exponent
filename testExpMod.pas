program testExpMod;
  uses sysutils, exponent;
  var
    a,n,m,run: Int64;
  begin
    a := StrToInt(ParamStr(1));
    n := StrToInt(ParamStr(2));
    m := StrToInt(ParamStr(3));
    run := exponentFindModular(a,n,m);
    writeln(IntToStr(run));
  end.
