program testExpReal;
  uses sysutils, Math, exponent;
  var
    a,n,run: Float;
  begin
    a := StrToFloat(ParamStr(1));
    n := StrToFloat(ParamStr(2));
    run := exponentFindReal(a,n);
    writeln(FormatFloat('0.00', run));
  end.
